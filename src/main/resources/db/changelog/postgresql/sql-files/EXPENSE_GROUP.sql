insert into expense_group (id, name) values (1, 'Food');
insert into expense_group (id, name) values (2, 'Gifts');
insert into expense_group (id, name) values (3, 'Clothes');
insert into expense_group (id, name) values (4, 'Transport');
insert into expense_group (id, name) values (5, 'Entertainment');
insert into expense_group (id, name) values (6, 'Hobby');
insert into expense_group (id, name) values (7, 'Travel');

SELECT pg_catalog.setval('expense_group_id_seq', (SELECT MAX(id)  FROM expense_group), true);
