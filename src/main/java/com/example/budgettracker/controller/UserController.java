package com.example.budgettracker.controller;

import com.example.budgettracker.facade.UserFacade;
import com.example.budgettracker.form.UserForm;
import com.example.budgettracker.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers(){
        List<User> users = userFacade.findAll();
        if (users.isEmpty()) {
            return new ResponseEntity<>(users, HttpStatus.NO_CONTENT);
        }
        return  new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody UserForm userForm){
        userFacade.save(userForm);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<Void> updateUser(@RequestBody UserForm userForm, @PathVariable(name="userId") Long userId){
        if(!userFacade.existById(userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userFacade.update(userForm, userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser (@PathVariable(name="userId") Long userId){
        if(!userFacade.existById(userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userFacade.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<User> getUser(@PathVariable(name="userId") Long userId){
        if(!userFacade.existById(userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = userFacade.findById(userId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
