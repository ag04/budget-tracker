package com.example.budgettracker.controller;

import com.example.budgettracker.converter.TransactionMapper;
import com.example.budgettracker.dto.TransactionDto;
import com.example.budgettracker.facade.ExpenseGroupFacade;
import com.example.budgettracker.facade.TransactionFacade;
import com.example.budgettracker.facade.UserFacade;
import com.example.budgettracker.form.TransactionForm;
import com.example.budgettracker.model.Transaction;
import com.example.budgettracker.security.CurrentUser;
import com.example.budgettracker.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/transactions")
public class TransactionController {

    private TransactionFacade transactionFacade;
    private UserFacade userFacade;
    private ExpenseGroupFacade expenseGroupFacade;
    private TransactionMapper transactionMapper;

    @Autowired
    public TransactionController(TransactionFacade transactionFacade, UserFacade userFacade, ExpenseGroupFacade expenseGroupFacade, TransactionMapper transactionMapper) {
        this.transactionFacade = transactionFacade;
        this.userFacade = userFacade;
        this.expenseGroupFacade = expenseGroupFacade;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping
    public ResponseEntity<List<Transaction> > getTransactions(@CurrentUser UserPrincipal currentUser){
        List<Transaction> transactions = transactionFacade.findAll();
            if(transactions.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<TransactionDto> > getTransactionsByUser(@CurrentUser UserPrincipal currentUser, @PathVariable(name="userId") Long userId){
        if(!userFacade.existById(userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Transaction> transactions = transactionFacade.findAllUserTransaction(userId);
        List<TransactionDto> transactionDtos = transactionMapper.mapFromEntityList(transactions);
        return new ResponseEntity<>(transactionDtos, HttpStatus.OK);
    }


    @GetMapping("/{transactionId}")
    public ResponseEntity<Transaction> getTransaction(@PathVariable(name="transactionId") Long transactionId){
        if(!transactionFacade.existById(transactionId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Transaction transaction = transactionFacade.findById(transactionId);
        return new ResponseEntity<>(transaction, HttpStatus.OK);
    }

    @GetMapping("/expense-group/{expenseGroupId}/user/{userId}")
    public ResponseEntity<List<TransactionDto>> getTransactionsByExpenseGroupAndUserId(@PathVariable(name="expenseGroupId") Long expenseGroupId, @PathVariable(name="userId") Long userId){
        if(!userFacade.existById(userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(!expenseGroupFacade.existById(expenseGroupId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Transaction> transactions = transactionFacade.findByUserAndExpenseGroupId(expenseGroupId, userId);
        List<TransactionDto> transactionDtos = transactionMapper.mapFromEntityList(transactions);
        return new ResponseEntity<>(transactionDtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TransactionDto> createTransaction(@CurrentUser UserPrincipal currentUser, @RequestBody TransactionForm transactionForm ){
        if(!userFacade.existById(transactionForm.getUserId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Transaction transaction = transactionFacade.save(transactionForm);
        TransactionDto transactionDto = transactionMapper.mapFromEntity(transaction);

        return new ResponseEntity<>(transactionDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{transactionId}")
    public ResponseEntity<Void> deleteTransaction(@PathVariable(name="transactionId") Long transactionId){
        if(!transactionFacade.existById(transactionId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        transactionFacade.delete(transactionId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
