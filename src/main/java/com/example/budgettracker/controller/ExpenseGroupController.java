package com.example.budgettracker.controller;


import com.example.budgettracker.dto.ExpenseGroupDto;
import com.example.budgettracker.facade.ExpenseGroupFacade;
import com.example.budgettracker.form.ExpenseGroupForm;
import com.example.budgettracker.security.CurrentUser;
import com.example.budgettracker.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/expense-groups")
public class
ExpenseGroupController {

    private ExpenseGroupFacade expenseGroupFacade;
    @Autowired
    public ExpenseGroupController(ExpenseGroupFacade expenseGroupFacade) {
        this.expenseGroupFacade = expenseGroupFacade;
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<ExpenseGroupDto>> getExpenseGroupsByUser(@CurrentUser UserPrincipal currentUser, @PathVariable(name="userId") Long userId){
        List<ExpenseGroupDto> expenseGroupDtos = expenseGroupFacade.findAll(userId);
        return new ResponseEntity<>(expenseGroupDtos, HttpStatus.OK);
    }

    @GetMapping("/{expenseGroupId}/{userId}")
    public ResponseEntity<ExpenseGroupDto> getExpenseGroupByUser(@PathVariable(name="expenseGroupId") Long expenseGroupId, @PathVariable(name="userId") Long userId){
        if(!expenseGroupFacade.existById(expenseGroupId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        ExpenseGroupDto expenseGroupDto = expenseGroupFacade.findById(expenseGroupId, userId);
        return new ResponseEntity<>(expenseGroupDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> createExpenseGroup(@RequestBody ExpenseGroupForm expenseGroupForm){
        expenseGroupFacade.save(expenseGroupForm);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{expenseGroupId}")
    public ResponseEntity<Void> updateExpenseGroup(@PathVariable(name="expenseGroupId") Long expenseGroupId, @RequestBody ExpenseGroupForm expenseGroupForm){
        if(!expenseGroupFacade.existById(expenseGroupId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        expenseGroupFacade.update(expenseGroupId, expenseGroupForm);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{expenseGroupId}")
    public ResponseEntity<Void> deleteExpenseGroup (@PathVariable(name="expenseGroupId") Long expenseGroupId){
        if(!expenseGroupFacade.existById(expenseGroupId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        expenseGroupFacade.delete(expenseGroupId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
