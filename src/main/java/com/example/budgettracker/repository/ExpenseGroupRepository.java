package com.example.budgettracker.repository;

import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExpenseGroupRepository  extends JpaRepository<ExpenseGroup, Long> {

//    @Query(
//            value = " select u.id, u.first_name, u.last_name, u.balance, u.username, u.password " +
//                    " from user_app u " +
//                    " order by u.id desc ",
//            nativeQuery = true
//    )
    Optional<ExpenseGroup> findById(Long expenseGroupId);
    void deleteById(Long expenseGroupId);
    boolean existsById(Long expenseGroupId);
}
