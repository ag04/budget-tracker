package com.example.budgettracker.repository;


import com.example.budgettracker.model.Transaction;
import com.example.budgettracker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Transaction save(Transaction transaction);
    List<Transaction> findAll();
    List<Transaction> findAllByUser(User user);
    boolean existsById(Long transactionId);


    @Query(
            value = "select * from transaction t" +
                    "        inner join expense_group eg on t.expense_group_id = eg.id" +
                    "        inner join user_app ua on t.user_id = ua.id" +
                    "        where ua.id = :userId and eg.id=:expenseGroupId",
            nativeQuery = true
    )
    List<Transaction> findByUserAndExpenseId( @Param("expenseGroupId") Long expenseGroupId, @Param("userId") Long userId);

    @Query(
            value = "select * from transaction t" +
                    " inner join user_app ua on t.user_id = ua.id" +
                    " where ua.id=:userId",
            nativeQuery = true
    )
    List<Transaction> findAllByUser(@Param("userId") Long userId);



}
