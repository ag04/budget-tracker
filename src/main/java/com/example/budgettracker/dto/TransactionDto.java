package com.example.budgettracker.dto;

import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.model.TransactionType;
import com.example.budgettracker.model.User;

public class TransactionDto {

    private Long id;
    private Long amount;
    private TransactionType transactionType;
    private User user;
    private ExpenseGroup expenseGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExpenseGroup getExpenseGroup() {
        return expenseGroup;
    }

    public void setExpenseGroup(ExpenseGroup expenseGroup) {
        this.expenseGroup = expenseGroup;
    }
}
