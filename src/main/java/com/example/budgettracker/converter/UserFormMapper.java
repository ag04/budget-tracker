package com.example.budgettracker.converter;

import com.example.budgettracker.form.UserForm;
import com.example.budgettracker.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserFormMapper {

    public User mapToEntity (UserForm userForm) {
        User user = new User();
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setUsername(userForm.getUsername());
        user.setPassword(userForm.getPassword());
        return user;
    }
}
