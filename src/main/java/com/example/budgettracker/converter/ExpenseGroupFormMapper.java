package com.example.budgettracker.converter;

import com.example.budgettracker.dto.ExpenseGroupDto;
import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.model.Transaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ExpenseGroupFormMapper {

    public ExpenseGroupDto mapFromEntity (ExpenseGroup expenseGroup, Long userId) {
        ExpenseGroupDto expenseGroupDto = new ExpenseGroupDto();

        expenseGroupDto.setId(expenseGroup.getId());
        expenseGroupDto.setName(expenseGroup.getName());
        expenseGroupDto.setTransactions(expenseGroup.getTransactions());
        expenseGroupDto.setTotal(countTotalExpenses(expenseGroup.getTransactions(), userId));
        return expenseGroupDto;
    }

    public List<ExpenseGroupDto> mapFromEntityList(List<ExpenseGroup> expenseGroups, Long userId){
        List<ExpenseGroupDto> expenseGroupsDto = new ArrayList<>();
        expenseGroups.forEach(eg -> {
            expenseGroupsDto.add(mapFromEntity(eg, userId));
        });

        return expenseGroupsDto;
    }

    private long countTotalExpenses (List<Transaction> transactions, Long userId) {
        List <Transaction> userTransactions = transactions.stream().filter(t -> t.getUser().getId() == userId).collect(Collectors.toList());
        List<Long> amounts = userTransactions.stream().map(Transaction::getAmount).collect(Collectors.toList());
        Long sum = amounts.stream().reduce(0L, Long::sum);

        return sum;
    }
}
