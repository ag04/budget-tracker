package com.example.budgettracker.converter;

import com.example.budgettracker.dto.TransactionDto;
import com.example.budgettracker.model.Transaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionMapper {

    public TransactionDto mapFromEntity (Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();

        transactionDto.setId(transaction.getId());
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setUser(transaction.getUser());
        transactionDto.setExpenseGroup(transaction.getExpenseGroup());
        transactionDto.setTransactionType(transaction.getTransactionType());
        return transactionDto;
    }

    public List<TransactionDto> mapFromEntityList(List<Transaction> transactions){
        List<TransactionDto> transactionDto = new ArrayList<>();
        transactions.forEach(t -> {
            transactionDto.add(mapFromEntity(t));
        });

        return transactionDto;
    }
}

