package com.example.budgettracker.service;

import com.example.budgettracker.model.ExpenseGroup;

import java.util.List;

public interface ExpenseGroupService {
    ExpenseGroup findById(Long expenseGroupId);
    void save (ExpenseGroup expenseGroup);
    List<ExpenseGroup> findAll();
    void delete (Long expenseGroupId);
    boolean existById (Long expenseGroupId);
}
