package com.example.budgettracker.service;

import com.example.budgettracker.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    void save(User user);
    User findById (Long userId);
    boolean existsById(Long userId);
    void delete (Long userId);
}
