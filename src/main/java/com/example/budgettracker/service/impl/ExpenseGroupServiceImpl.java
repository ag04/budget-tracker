package com.example.budgettracker.service.impl;

import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.repository.ExpenseGroupRepository;
import com.example.budgettracker.service.ExpenseGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseGroupServiceImpl implements ExpenseGroupService {

    private ExpenseGroupRepository expenseGroupRepository;

    @Autowired
    public ExpenseGroupServiceImpl(ExpenseGroupRepository expenseGroupRepository) {
        this.expenseGroupRepository = expenseGroupRepository;
    }

    @Override
    public ExpenseGroup findById(Long expenseGroupId) {
        ExpenseGroup expenseGroup = expenseGroupRepository.findById(expenseGroupId).get();
        return expenseGroup;
    }

    @Override
    public void save(ExpenseGroup expenseGroup) {
        expenseGroupRepository.save(expenseGroup);
    }

    @Override
    public List<ExpenseGroup> findAll() {
        return expenseGroupRepository.findAll();
    }

    @Override
    public void delete(Long expenseGroupId) {
        expenseGroupRepository.deleteById(expenseGroupId);
    }

    @Override
    public boolean existById(Long expenseGroupId) {
        return expenseGroupRepository.existsById(expenseGroupId);
    }
}
