package com.example.budgettracker.service.impl;

import com.example.budgettracker.model.Transaction;
import com.example.budgettracker.model.User;
import com.example.budgettracker.repository.TransactionRepository;
import com.example.budgettracker.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction save(Transaction transaction) {
        Transaction createdTransaction = transactionRepository.save(transaction);
        return createdTransaction;
    }

    @Override
    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    @Override
    public void delete(Long transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).get();
        transactionRepository.delete(transaction);
    }

    @Override
    public Transaction findById(Long transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).get();
        return transaction;
    }

    @Override
    public boolean existById(Long transactionId) {
        return transactionRepository.existsById(transactionId);
    }

    @Override
    public List<Transaction> findAllUserTransactions(Long userId) {
        return transactionRepository.findAllByUser(userId);
    }

    @Override
    public List<Transaction> findByUserAndExpenseGroupId(Long expenseGroupId, Long userId) {
        return transactionRepository.findByUserAndExpenseId(expenseGroupId, userId);
    }
}
