package com.example.budgettracker.service;

import com.example.budgettracker.model.Transaction;
import com.example.budgettracker.model.User;

import java.util.List;

public interface TransactionService {
    Transaction save (Transaction transaction);
    List<Transaction> findAll();
    void delete(Long transactionId);
    Transaction findById(Long transactionId);
    boolean existById(Long transactionId);
    List<Transaction> findAllUserTransactions(Long userId);
    List<Transaction> findByUserAndExpenseGroupId(Long expenseGroupId, Long userId);
}
