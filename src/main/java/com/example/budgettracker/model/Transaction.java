package com.example.budgettracker.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "TRANSACTION")
public class Transaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="AMOUNT")
    private long amount;

    @Enumerated(EnumType.STRING)
    @Column(name="TRANSACTION_TYPE")
    private TransactionType transactionType;

    @Column(name="DATE_TIME")
    LocalDateTime dateTime;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    @JsonBackReference
    private User user;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = ExpenseGroup.class)
    @JoinColumn(name = "expense_group_id")
    @JsonBackReference
    private ExpenseGroup expenseGroup;

    public Transaction() {
    }

    public Transaction(int amount, TransactionType transactionType, User user, ExpenseGroup expenseGroup) {
        this.amount = amount;
        this.transactionType = transactionType;
        this.user = user;
    }

    public Transaction(int amount) {
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExpenseGroup getExpenseGroup() {
        return expenseGroup;
    }

    public void setExpenseGroup(ExpenseGroup expenseGroup) {
        this.expenseGroup = expenseGroup;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
