package com.example.budgettracker.facade.impl;

import com.example.budgettracker.converter.TransactionMapper;
import com.example.budgettracker.dto.TransactionDto;
import com.example.budgettracker.facade.TransactionFacade;
import com.example.budgettracker.form.TransactionForm;
import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.model.Transaction;
import com.example.budgettracker.model.TransactionType;
import com.example.budgettracker.model.User;
import com.example.budgettracker.service.ExpenseGroupService;
import com.example.budgettracker.service.TransactionService;
import com.example.budgettracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TransactionFacadeImpl implements TransactionFacade {

    private TransactionService transactionService;
    private UserService userService;
    private ExpenseGroupService expenseGroupService;
    private TransactionMapper transactionMapper;

    @Autowired
    public TransactionFacadeImpl(TransactionService transactionService, UserService userService, ExpenseGroupService expenseGroupService, TransactionMapper transactionMapper) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.expenseGroupService=expenseGroupService;
        this.transactionMapper = transactionMapper;
    }

    @Override
    public Transaction save(TransactionForm transactionForm) {
        Transaction transaction = new Transaction();
        User user = userService.findById(transactionForm.getUserId());
        if(transactionForm.getExpenseGroupId() != 0) {
            ExpenseGroup expenseGroup = expenseGroupService.findById(transactionForm.getExpenseGroupId());
            transaction.setExpenseGroup(expenseGroup);

        }
        LocalDateTime dateTime = LocalDateTime.now();

        if (transactionForm.getTransactionType().equalsIgnoreCase("Income")) {
            transaction.setTransactionType(TransactionType.INCOME);
        }
        else if (transactionForm.getTransactionType().equalsIgnoreCase("OUTCOME")) {
            transaction.setTransactionType(TransactionType.OUTCOME);
        }

        transaction.setDateTime(dateTime);
        transaction.setUser(user);
        transaction.setAmount(transactionForm.getAmount());
        transaction.setTransactionType(transaction.getTransactionType());
        user.setBalance(calculateBalance(user.getBalance(), transactionForm.getAmount(), transactionForm.getTransactionType()));

        userService.save(user);
        transactionService.save(transaction);
       return transaction;
    }

    @Override
    public List<Transaction> findAll() {
        return transactionService.findAll();
    }

    private long calculateBalance (long balance, long amount, String transactionType) {
        long result = 0;
        if (transactionType.equalsIgnoreCase("Outcome")) {
            result = balance - amount;
        }
        else if (transactionType.equalsIgnoreCase("Income")) {
            result = balance + amount;
        }
        return result;
    }

    @Override
    public Transaction findById(Long transactionId) {
        return transactionService.findById(transactionId);
    }

    @Override
    public boolean existById(Long transactionId) {
        return transactionService.existById(transactionId);
    }

    @Override
    public List<Transaction> findAllUserTransaction(Long userId) {
        return transactionService.findAllUserTransactions(userId);
    }

    @Override
    public void delete(Long transactionId) {
        transactionService.delete(transactionId);
    }

    @Override
    public List<Transaction> findByUserAndExpenseGroupId(Long expenseGroupId, Long userId) {
        return transactionService.findByUserAndExpenseGroupId(expenseGroupId, userId);
    }
}
