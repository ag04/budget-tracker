package com.example.budgettracker.facade.impl;

import com.example.budgettracker.converter.UserFormMapper;
import com.example.budgettracker.facade.UserFacade;
import com.example.budgettracker.form.UserForm;
import com.example.budgettracker.model.User;
import com.example.budgettracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserFacadeImpl implements UserFacade {

    private UserService userService;
    private UserFormMapper userFormMapper;

    @Autowired
    public UserFacadeImpl(UserService userService, UserFormMapper userFormMapper) {
        this.userService = userService;
        this.userFormMapper = userFormMapper;
    }

    @Override
    public void save(UserForm userForm) {
      userService.save(userFormMapper.mapToEntity(userForm));
    }

    @Override
    public List<User> findAll() {
        return userService.findAll();
    }

    @Override
    public void update(UserForm userForm, Long userId) {
            User user = userService.findById(userId);
            user.setFirstName(userForm.getFirstName());
            user.setLastName(userForm.getLastName());
            user.setUsername(userForm.getUsername());
            user.setPassword(userForm.getPassword());
            userService.save(user);
    }

    @Override
    public boolean existById(Long userId) {
        return userService.existsById(userId);
    }

    @Override
    public User findById(Long userId) {
        return userService.findById(userId);
    }

    @Override
    public void delete(Long userId) {
        userService.delete(userId);
    }
}
