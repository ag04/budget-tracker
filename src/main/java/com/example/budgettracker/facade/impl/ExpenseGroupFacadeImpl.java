package com.example.budgettracker.facade.impl;

import com.example.budgettracker.converter.ExpenseGroupFormMapper;
import com.example.budgettracker.dto.ExpenseGroupDto;
import com.example.budgettracker.facade.ExpenseGroupFacade;
import com.example.budgettracker.form.ExpenseGroupForm;
import com.example.budgettracker.model.ExpenseGroup;
import com.example.budgettracker.service.ExpenseGroupService;
import com.example.budgettracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ExpenseGroupFacadeImpl implements ExpenseGroupFacade {

    private ExpenseGroupService expenseGroupService;
    private UserService userService;
    private ExpenseGroupFormMapper expenseGroupFormMapper;

    @Autowired
    public ExpenseGroupFacadeImpl(ExpenseGroupService expenseGroupService, UserService userService, ExpenseGroupFormMapper expenseGroupFormMapper) {
        this.expenseGroupService = expenseGroupService;
        this.userService = userService;
        this.expenseGroupFormMapper = expenseGroupFormMapper;
    }

    @Override
    public void save(ExpenseGroupForm expenseGroupForm) {
        ExpenseGroup expenseGroup = new ExpenseGroup();
        expenseGroup.setName(expenseGroupForm.getName());
        expenseGroupService.save(expenseGroup);
    }

    @Override
    public List<ExpenseGroupDto> findAll(Long userId) {
        List<ExpenseGroup> expenseGroups = expenseGroupService.findAll();

        List<ExpenseGroupDto> expenseGroupsDto = expenseGroupFormMapper.mapFromEntityList(expenseGroups, userId);
        return expenseGroupsDto;
    }

    @Override
    public void delete(Long expenseGroupId) {
        expenseGroupService.delete(expenseGroupId);
    }

    @Override
    public void update(Long expenseGroupId, ExpenseGroupForm expenseGroupForm) {
             ExpenseGroup expenseGroup = expenseGroupService.findById(expenseGroupId);
             expenseGroup.setName(expenseGroupForm.getName());
             expenseGroupService.save(expenseGroup);
    }

    @Override
    public boolean existById(Long expenseGroupId) {
        return expenseGroupService.existById(expenseGroupId);
    }

    @Override
    public ExpenseGroupDto findById(Long expenseGroupId, Long userId) {
        ExpenseGroup expenseGroup = expenseGroupService.findById(expenseGroupId);
        ExpenseGroupDto expenseGroupDto = expenseGroupFormMapper.mapFromEntity(expenseGroup, userId);
        return expenseGroupDto;
    }
}
