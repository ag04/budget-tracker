package com.example.budgettracker.facade;

import com.example.budgettracker.dto.ExpenseGroupDto;
import com.example.budgettracker.form.ExpenseGroupForm;
import com.example.budgettracker.model.ExpenseGroup;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ExpenseGroupFacade {
    void save(ExpenseGroupForm expenseGroupForm);
    List<ExpenseGroupDto> findAll(Long userId);
    void delete(Long expenseGroupId);
    void update (Long expenseGroupId, ExpenseGroupForm expenseGroupForm);
    boolean existById(Long expenseGroupId);
    ExpenseGroupDto findById(Long expenseGroupId, Long userId);
}
