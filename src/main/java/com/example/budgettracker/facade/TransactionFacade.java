package com.example.budgettracker.facade;

import com.example.budgettracker.form.TransactionForm;
import com.example.budgettracker.model.Transaction;

import java.util.List;

public interface TransactionFacade {
    Transaction save(TransactionForm transactionForm);
    List<Transaction> findAll();
    Transaction findById(Long transactionId);
    boolean existById(Long transactionId);
    List<Transaction> findAllUserTransaction(Long userId);
    void delete(Long transactionId);
    List<Transaction> findByUserAndExpenseGroupId(Long expenseGroupId, Long userId);
}
