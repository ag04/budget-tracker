package com.example.budgettracker.facade;

import com.example.budgettracker.form.UserForm;
import com.example.budgettracker.model.User;

import java.util.List;

public interface UserFacade {
    void save(UserForm userForm);
    List<User> findAll();
    void update(UserForm userForm, Long userId);
    boolean existById(Long userId);
    void delete(Long userId);
    User findById(Long userId);
}
